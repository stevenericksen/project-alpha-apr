from django import forms
from tasks.models import Task
from django.forms import DateInput


class DateInput(DateInput):
    input_type = "date"


class TaskForm(forms.ModelForm):
    class Meta:
        model = Task
        fields = [
            "name",
            "start_date",
            "due_date",
            "project",
            "assignee",
        ]
        widgets = {
            "start_date": DateInput,
            "due_date": DateInput,
        }
